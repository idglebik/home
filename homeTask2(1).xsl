<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="xs"
                version="1.0"
                xmlns:r="RoomsChema"
                xmlns:h="HouseChema"
                xmlns:i="HouseInfo">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />  
        <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" /> <!--Для перевода города в Uppercase-->
        <AllRooms>
            <xsl:for-each select="//r:Room">                                    <!--Цикл по всем Room-->
                <xsl:sort select="ancestor-or-self::h:House/@City"/>            <!--Сортируем по городу-->
                <xsl:sort select="ancestor::h:Block/@number"/>                  <!--Сортируем по номеру блока-->
                <xsl:sort select="./@nuber"/>                                   <!--Сортируем по номеру комнаты-->
                <Room>
                <Address>
                 <!--Задаём тэг Address и составляем строку согласно заданию-->
                <xsl:value-of select="translate(ancestor-or-self::h:House/@City,$smallcase,$uppercase)"/>/<xsl:value-of select="ancestor-or-self::h:House/i:Address"/>/<xsl:value-of select="ancestor::h:Block/@number"/>/<xsl:value-of select="./@nuber"/>
                </Address>
            <HouseRoomsCount>
                <xsl:value-of select="count(ancestor-or-self::h:House//r:Room)"/><!--Количество комнат во всех блоках во всем доме-->  
            </HouseRoomsCount>
            <BlockRoomsCount>
                <xsl:value-of select="count(ancestor::h:Block//r:Room)"/>        <!--Количество комнат в блоке--> 
            </BlockRoomsCount>
            <HouseGuestsCount>
                <xsl:value-of select="sum(ancestor-or-self::h:House//r:Room/@guests)"/> <!-- Общее количество гостей в доме -->
            </HouseGuestsCount>
            <GuestsPerRoomAverage>
                <!-- Среднее количество гостей на 1 комнату - наименьшее целое -->
                <xsl:value-of select="floor(sum(ancestor-or-self::h:House//r:Room/@guests) div count(ancestor-or-self::h:House//r:Room))"/>
            </GuestsPerRoomAverage>
                    <!--Ветвление для задания Аllocated-->
                <xsl:choose>
                    <xsl:when test="@guests = 1">
                        <Allocated Single="true" Double="false" Triple="false" Quarter="false"/>
                    </xsl:when >
                    <xsl:when test="@guests = 2">
                    <Allocated Single="false" Double="true" Triple="false" Quarter="false"/>
                </xsl:when>
                    <xsl:when test="@guests = 3">
                        <Allocated Single="false" Double="false" Triple="true" Quarter="false"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <Allocated Single="false" Double="false" Triple="false" Quarter="true"/>
                    </xsl:otherwise>
                </xsl:choose>
                </Room>
            </xsl:for-each>
        </AllRooms>
    </xsl:template>
</xsl:stylesheet>