<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:key name="gis" match="*" use="name()"/>              
    <xsl:key name="atrAndEl" match="*|@*" use="name()"/>
    <xsl:template match="/">
        <xsl:apply-templates/>
        <xsl:message>done reading</xsl:message>
        <xsl:for-each select="//*[generate-id(.)=generate-id(key('gis',name(.))[1])]">
            <xsl:sort select="name()"/>
            <xsl:text>Node '</xsl:text><xsl:value-of select="name(.)"/><xsl:text>' found </xsl:text><xsl:value-of select="count(key('atrAndEl',name(.)))"/><xsl:text> times</xsl:text>
            <xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="text()"/>
</xsl:stylesheet>