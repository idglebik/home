<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ext="http://exslt.org/common" exclude-result-prefixes="ext">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    
    <xsl:variable name="string">                                
        <xsl:for-each select="//Guest">
            <xsl:value-of select="concat(@*[1],'|',@*[2],'|',@*[3],'|',@*[4],'|',self::Guest/Type, '|', self::Guest/Profile/Address,'|')"/>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="vrtfSplitWords">                        <!-- Сплитим строку и запоминаем в переменную -->
        <xsl:call-template name="split">
            <xsl:with-param name="pText" select="$string"/>
        </xsl:call-template>
    </xsl:variable>
    
    <xsl:template match="/">
        <Guests>
       <xsl:call-template name="for"/>                          <!--Темплейт для распределения элементов и атрибутов по местам-->
        </Guests>
    </xsl:template>
    
    <xsl:template name="split">
        
        <xsl:param name="pText" select="$string"/>
        <xsl:param name="pDelim" select="'|'"/>
        <xsl:param name="pElemName" select="'word'"/>
        
        <xsl:if test="string-length($pText)">
            <xsl:element name="{$pElemName}">
                <xsl:value-of select=
                    "substring-before(concat($pText,$pDelim),
                    $pDelim
                    )
                    "/>
            </xsl:element>
            
            <xsl:call-template name="split">
                <xsl:with-param name="pText" select=
                    "substring-after($pText,$pDelim)"/>
                <xsl:with-param name="pDelim" select="$pDelim"/>
                <xsl:with-param name="pElemName" select="$pElemName"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="for">
        <xsl:param name="i" select="1"/>
        <xsl:param name="n" select="count(ext:node-set($vrtfSplitWords)/*)"/>
        <xsl:if test="$i &lt; $n">
            <Guest>
                <xsl:attribute name="Age">
                    <xsl:value-of select="ext:node-set($vrtfSplitWords)/*[number($i)]"/>
                </xsl:attribute>
                <xsl:attribute name="Nationalty">
                    <xsl:value-of select="ext:node-set($vrtfSplitWords)/*[number($i)+1]"/>
                </xsl:attribute>
                <xsl:attribute name="Gender">
                    <xsl:value-of select="ext:node-set($vrtfSplitWords)/*[number($i)+2]"/>
                </xsl:attribute>
                <xsl:attribute name="Name">
                    <xsl:value-of select="ext:node-set($vrtfSplitWords)/*[number($i)+3]"/>
                </xsl:attribute>
                <Type>
                    <xsl:value-of select="ext:node-set($vrtfSplitWords)/*[number($i)+4]"/>
                </Type>
                <Profile>
                    <Address><xsl:value-of select="ext:node-set($vrtfSplitWords)/*[number($i)+5]"/></Address>
                </Profile>
            </Guest>
            <xsl:call-template name="for">
                <xsl:with-param name="i" select="$i + 6"/>
                <xsl:with-param name="n" select="count(ext:node-set($vrtfSplitWords)/*)"/>
            </xsl:call-template> 
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>