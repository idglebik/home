<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ext="http://exslt.org/common" exclude-result-prefixes="ext">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    
    <xsl:template match="/">
        <list>
        <xsl:call-template name="for">
            <xsl:with-param name="n" select="count(//item)+1"/>
        </xsl:call-template>
        </list>
    </xsl:template>

    <xsl:template name="for" match="//item">
        <xsl:param name="i" select="1"/>
        <xsl:param name="n"/>
       <xsl:param name="sub" select="'sub'"/>                                           
      <!--Логика: берём первый элемент, получаем его букву, забиваем её в атрибут value ищем
        Элемент с такой же первой буквой и наполняем <names>. Далее запоминаем в переменную sub
        эту букву и при следующем вызове(рекурсии) будем её сравнивать с текущей буквой цикла. Таким образом
        <capital> создастся только для буквы, которая есть-->
        <xsl:if test="$i &lt; $n">                                              
            <xsl:if test="$sub != substring(ext:node-set(//@Name)[$i],1,1)">               
                <capital><xsl:attribute name="value"><xsl:value-of select="substring(ext:node-set(//@Name)[$i],1,1)"/></xsl:attribute> 
                    <xsl:for-each select="//@Name">
                        <xsl:if test="substring(.,1,1) = substring(ext:node-set(//@Name)[$i],1,1)">
                          <name><xsl:value-of select="."/></name>
                        </xsl:if>
                    </xsl:for-each>
                </capital> 
            </xsl:if>              
                 
            <xsl:call-template name="for">
                <xsl:with-param name="sub" select="substring(ext:node-set(//@Name)[$i],1,1)"/>
                <xsl:with-param name="i" select="$i + 1"/>
                <xsl:with-param name="n" select="$n"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>