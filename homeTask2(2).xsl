<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:str="http://exslt.org/strings"
                xmlns:exsl="http://exslt.org/common" version="2.0"
                exclude-result-prefixes="exsl str" extension-element-prefixes="exsl str">
    <xsl:output method="text"/>
    <xsl:output method="xml" indent="yes"/>


    <xsl:template match="/">
        <xsl:variable name="string">                                <!--В переменную string будем сохранять concat атрибутов и текстовых node в виде 25|BY|M|Adult|Pushkinskaya 26-.....-->
            <xsl:for-each select="//Guest">
                <xsl:value-of select="concat(@*[1],'|',@*[2],'|',@*[3],'|',@*[4],'|',self::Guest/Type, '|', self::Guest/Profile/Address,'-')"/>
            </xsl:for-each>
        </xsl:variable>
        <!--Востанавлимаем XML из переменной string-->
        <xsl:variable name="first">
            <xsl:for-each select="str:tokenize($string,'-')">         <!--Разделитель '-' отделяет одного Guest от другого, поэтому в начале делим строку по '-'-->
                <temp>
                    <xsl:value-of select="."/>
                </temp>
            </xsl:for-each>
        </xsl:variable>
        <xsl:for-each select="exsl:node-set($first)/temp">        <!-- Этот nodeset cодержит каждого Guest отдельно -->
            <Guest>
                <xsl:variable name="second">
                    <xsl:for-each select="str:tokenize(.,'\|')">      <!-- Делим строку по | для получения отельных текстовых node и атрибутов -->
                        <temp2>
                            <xsl:value-of select="."/>
                        </temp2>
                    </xsl:for-each>
                </xsl:variable>
                <!-- Зная номер каждого элемента, составлеяем тэги  -->
                <xsl:attribute name="Age">
                    <xsl:value-of select="exsl:node-set($second)/temp2[1]"/>
                </xsl:attribute>
                <xsl:attribute name="Nationalty">
                    <xsl:value-of select="exsl:node-set($second)/temp2[2]"/>
                </xsl:attribute>
                <xsl:attribute name="Gender">
                    <xsl:value-of select="exsl:node-set($second)/temp2[3]"/>
                </xsl:attribute>
                <xsl:attribute name="Name">
                    <xsl:value-of select="exsl:node-set($second)/temp2[4]"/>
                </xsl:attribute>
                <Type>
                    <xsl:value-of select="exsl:node-set($second)/temp2[5]"/>
                </Type>
                <Profile>
                    <Address><xsl:value-of select="exsl:node-set($second)/temp2[6]"/></Address>
                </Profile>
            </Guest>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>